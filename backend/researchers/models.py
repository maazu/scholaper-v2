from django.db import models

from university.models import Univerisy


class Researcher(models.Model):
    class Meta:
        db_table = 'researcher'
        verbose_name_plural = "Researchers"

    name = models.CharField(max_length=500, null=False,
                            blank=False)

    univerisy = models.ForeignKey(Univerisy, on_delete=models.SET_DEFAULT, default='',
                                  null=False, blank=False)

    scholar_id = models.CharField(max_length=4000, null=True,
                                  blank=True)

    emails = models.CharField(max_length=1000, null=True,
                              blank=True)

    paper_scrap = models.BooleanField(default=False)

    def __str__(self):
        return self.name + " | " + self.univerisy
