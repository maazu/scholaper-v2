from django.contrib import admin

from researchers.models import Researcher

admin.site.register(Researcher)
