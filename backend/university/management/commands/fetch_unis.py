from bs4 import BeautifulSoup
import re
import requests
import pandas as pd
from django.core.management.base import BaseCommand
from university.models import Univerisy


class Command(BaseCommand):
    TOP_RANKING_UNI_URL = "https://www.webometrics.info/en/"

    def handle(self, *args, **options):
        self.extract_uni_data()

    def prepare_url(self, continent, country):
        webometrics_url = self.TOP_RANKING_UNI_URL + \
            str(continent) + "/" + country
        print("webometrics url ==> ", webometrics_url)
        return webometrics_url

    def get_page_code(self, url):
        r = requests.get(url)
        soup = BeautifulSoup(r.text, 'lxml')
        table = soup.find('table', {'class': 'sticky-enabled'})
        a_tags = table.find_all('a', text=True, attrs={
                                'href': re.compile("^https|http://")})
        return a_tags

    def filter_uni_domain(self, domain):
        if domain.endswith('/'):
            domain = domain[:-1]
        if domain.startswith('https://'):
            domain = domain[8:]
        if domain.startswith('http://'):
            domain = domain[7:]
        if domain.startswith('www'):
            domain = domain[3:]
        if domain.startswith('.'):
            domain = domain[1:]
        return domain

    def extract_uni_data(self):
        top_ranked_limit = 35
        countries_df = pd.read_csv('countries.csv', error_bad_lines=False, )
        countries_df = countries_df.drop_duplicates()
        for index, row in countries_df.iterrows():
            country = row['country']
            continent = row['continent']
            url = self.prepare_url(continent, country)
            a_tags = self.get_page_code(url)
            rank = 0
            for a_tag in a_tags:
                rank = rank + 1
                uni_link = self.filter_uni_domain(a_tag.get('href'))
                uni_name = a_tag.text
                Univerisy(name=uni_name, country=country,
                          web_address=uni_link, rank=rank).save()

                if rank == top_ranked_limit:
                    break
