from django.db import models


class Univerisy(models.Model):
    class Meta:
        db_table = 'univeristy'
        verbose_name_plural = "universities"

    name = models.CharField(max_length=250, null=False,
                            blank=False)
    country = models.CharField(max_length=100, null=False,
                               blank=False)

    rank = models.IntegerField(null=True, default=0)

    web_address = models.CharField(default='Unknown',
                                   max_length=100, null=False, blank=False)

    scrapped = models.BooleanField(default=False)

    def __str__(self):
        return self.name + " | " + self.country
