import os
import sys
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import WebDriverException
from psycopg2.extensions import adapt
from utils.presistance_tools.database.repository.scholars_paper import ScholarPapers
from component.chrome_driver.chrome_driver import get_driver_options, get_driver_directory
from component.proxy_loader.proxy_bouncer import reload_fresh_proxy, update_multiple_expired_status


def filtered(element, word):
    try:
        element = element.split(word)[1]
        return element
    except Exception as e:
        element = 0
        return element


def quit_brower(chrome_browser):
    try:
        chrome_browser.quit()
    except Exception as e:
        print("Browser already closed !")


def check_proxy_count(proxies):
    if len(proxies) == 0:
        print("out of proxies")
        sys.exit()


def start_reference_scraping(db_connection):
    print("Reference thread is running....")
    scholar_data = ScholarPapers(db_connection)
    progress = ScholarPapers(db_connection)
    options = get_driver_options()
    proxies = reload_fresh_proxy(db_connection)
    check_proxy_count(proxies)

    options.add_argument('--proxy-server={}'.format(proxies[0]))
    chrome_browser = webdriver.Chrome(options=options, executable_path=get_driver_directory())
    last_progress = progress.get_scholars_id_list()
    proxy_count = 0

    for scholar_id in last_progress:
        scholar_profile_link = "https://scholar.google.com/citations?hl=en&user=" + scholar_id
        print("Moving to next scholar ID ==> {} \nUrl: {}".format(scholar_id,scholar_profile_link))
        while True:

            if proxy_count == 29:
                quit_brower(chrome_browser)
                print("Updating proxy expired list")
                update_multiple_expired_status(proxies, db_connection)
                proxies = reload_fresh_proxy(db_connection)
                check_proxy_count(proxies)
                proxy_count = 0
                options.add_argument('--proxy-server={}'.format(proxies[proxy_count]))
                chrome_browser = webdriver.Chrome(options=options, executable_path=get_driver_directory())

            try:

                chrome_browser.get(scholar_profile_link)

                chrome_browser.set_page_load_timeout(50)

                if 'sorry' in chrome_browser.current_url:
                    print("Bot Protection Url detected...")
                    proxy_count = proxy_count + 1
                    quit_brower(chrome_browser)
                    options.add_argument('--proxy-server={}'.format(proxies[proxy_count]))
                    chrome_browser = webdriver.Chrome(options=options, executable_path=get_driver_directory())

                elif 'Sorry' in chrome_browser.title:
                    print("bot detection url.........")
                    proxy_count = proxy_count + 1
                    chrome_browser.quit()
                    options.add_argument('--proxy-server={}'.format(proxies[proxy_count]))
                    chrome_browser = webdriver.Chrome(options=options, executable_path=get_driver_directory())

                else:
                    WebDriverWait(chrome_browser, 10).until(EC.title_contains("Google Scholar"))
                    paper_publish_year_object = chrome_browser.find_elements_by_xpath(
                        "//span[@class='gsc_a_h gsc_a_hc gs_ibl']")
                    paper_citations_object = chrome_browser.find_elements_by_xpath("//a[@class='gsc_a_ac gs_ibl']")
                    paper_reference_link_object = chrome_browser.find_elements_by_xpath("//a[@class='gsc_a_at']")
                    paper_reference_links = [elem.get_attribute('href') for elem in paper_reference_link_object]
                    paper_reference_titles = [elem.text for elem in paper_reference_link_object]
                    paper_citations = [elem.text for elem in paper_citations_object]
                    paper_publish_years = [elem.text for elem in paper_publish_year_object]

                    for title, link, cites, year in zip(paper_reference_titles, paper_reference_links, paper_citations,
                                                        paper_publish_years):
                        if cites == "" or cites is None:
                            cites = 0
                        if year == "" or cites is None:
                            year = 0

                        title = title.replace("'", "%")
                        query = "INSERT INTO scholars_paper(scholar_id, title, cited_by, publish_year, reference_url)  VALUES ('{}','{}',{},{},'{}');".format(
                                scholar_id, title, cites, year, link)
                        progress.insert_new_paper(query)

                    break


            except TimeoutException as ex:
                print("Time out exception while loading page {}".format(scholar_profile_link))
                print("proxy expired iteration count ==> {} \nProxy address expired: {}".format(str(proxy_count),
                                                                                                str(proxies[
                                                                                                        proxy_count])))
                proxy_count = proxy_count + 1
                quit_brower(chrome_browser)
                options.add_argument('--proxy-server={}'.format(proxies[proxy_count]))
                chrome_browser = webdriver.Chrome(options=options, executable_path=get_driver_directory())

            except WebDriverException as ex:
                print("Web Driver exception while loading page {}".format(scholar_profile_link))
                print("Proxy expired iteration count ==> {} \nProxy address expired: {}".format(str(proxy_count),
                                                                                                str(proxies[
                                                                                                        proxy_count])))
                proxy_count = proxy_count + 1
                quit_brower(chrome_browser)
                options.add_argument('--proxy-server={}'.format(proxies[proxy_count]))
                chrome_browser = webdriver.Chrome(options=options, executable_path=get_driver_directory())


