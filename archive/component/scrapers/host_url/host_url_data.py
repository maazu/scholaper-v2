import os
import sys
import time
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import WebDriverException
from utils.presistance_tools.database.repository.scholars_paper import ScholarPapers
from component.chrome_driver.chrome_driver import get_driver_options, get_driver_directory
from component.proxy_loader.proxy_bouncer import reload_fresh_proxy, update_multiple_expired_status


def filtered(element, word):
    try:
        element = element.split(word)[1]
        return element
    except Exception as e:
        element = 0
        return element


def quit_brower(chrome_browser):
    try:
        chrome_browser.quit()
    except Exception as e:
        print("Browser already closed !")


def check_proxy_count(proxies):
    if len(proxies) == 0:
        print("out of proxies")
        sys.exit()


def start_host_url_scraping(db_connection):
    print("Host Url thread is running....")
    # getting the progress from database, last progress is where host link is null
    progress = ScholarPapers(db_connection)
    options = get_driver_options()
    proxies = reload_fresh_proxy(db_connection)
    check_proxy_count(proxies)

    options.add_argument('--proxy-server={}'.format(proxies[0]))
    chrome_browser = webdriver.Chrome(options=options, executable_path=get_driver_directory())
    last_progress = progress.get_reference_url()
    proxy_count = 0

    for reference_url in last_progress:

        print("Extracting Paper Host url from Reference Url ==> {}".format(reference_url))
        while True:
            if proxy_count == 29:
                quit_brower(chrome_browser)
                print("Updating proxy expired list")
                update_multiple_expired_status(proxies, db_connection)
                proxies = reload_fresh_proxy(db_connection)
                check_proxy_count(proxies)
                proxy_count = 0
                options.add_argument('--proxy-server={}'.format(proxies[proxy_count]))
                chrome_browser = webdriver.Chrome(options=options, executable_path=get_driver_directory())

            try:

                chrome_browser.get(reference_url)
                chrome_browser.set_page_load_timeout(50)

                if 'sorry' in chrome_browser.current_url:
                    print("Bot Protection Url detected...")
                    proxy_count = proxy_count + 1
                    quit_brower(chrome_browser)
                    options.add_argument('--proxy-server={}'.format(proxies[proxy_count]))
                    chrome_browser = webdriver.Chrome(options=options, executable_path=get_driver_directory())

                elif 'Sorry' in chrome_browser.title:
                    print("bot detection url.........")
                    proxy_count = proxy_count + 1
                    chrome_browser.quit()
                    options.add_argument('--proxy-server={}'.format(proxies[proxy_count]))
                    chrome_browser = webdriver.Chrome(options=options, executable_path=get_driver_directory())

                else:
                    WebDriverWait(chrome_browser, 30).until(EC.title_contains("View article"))
                    WebDriverWait(chrome_browser, 30).until(EC.visibility_of_element_located((By.ID, "gs_bdy")))
                    paper_host_link = ""
                    try:
                        # Trying to get right side link
                        paper_host_link_a_tag = chrome_browser.find_element_by_xpath("//div[@class='gsc_oci_title_ggi']//a")
                        paper_host_link = paper_host_link_a_tag.get_attribute('href')
                    except Exception as e:
                        print("first link attribute does not exist {}", str(e))

                    if paper_host_link == "":
                        # Trying to get title link
                        try:
                            paper_host_link_a_tag = chrome_browser.find_element_by_xpath(
                                "//a[@class='gsc_oci_title_link']")
                            paper_host_link = paper_host_link_a_tag.get_attribute('href')
                        except Exception as e:
                            print("first link attribute does not exist {}", str(e))

                    if len(paper_host_link) < 1:
                        paper_host_link = "N/A"

                    paper_host_link = paper_host_link.replace("'", "%")
                    query = "UPDATE SCHOLARS_PAPER SET host_url = '{}' where reference_url = '{}';".format(
                            paper_host_link, reference_url)

                    progress.scholar_paper_execute_query(query)
                    break


            except TimeoutException as ex:
                print("Time out exception while loading page {}".format(reference_url))
                print("proxy expired iteration count ==> {} \nProxy address expired: {}".format(str(proxy_count),
                                                                                                str(proxies[
                                                                                                        proxy_count])))
                proxy_count = proxy_count + 1
                quit_brower(chrome_browser)
                options.add_argument('--proxy-server={}'.format(proxies[proxy_count]))
                chrome_browser = webdriver.Chrome(options=options, executable_path=get_driver_directory())

            except WebDriverException as ex:
                print(ex)
                print("Web Driver exception while loading page {}".format(reference_url))
                print("Proxy expired iteration count ==> {} \nProxy address expired: {}".format(str(proxy_count),
                                                                                                str(proxies[
                                                                                                        proxy_count])))
                proxy_count = proxy_count + 1
                quit_brower(chrome_browser)
                options.add_argument('--proxy-server={}'.format(proxies[proxy_count]))
                chrome_browser = webdriver.Chrome(options=options, executable_path=get_driver_directory())
