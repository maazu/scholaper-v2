-- noinspection SqlDialectInspectionForFile

CREATE DATABASE 'scholaper_v2';

SET TIME ZONE 'Asia/Karachi';

CREATE TABLE proxy (
	proxy_id serial PRIMARY KEY,
	address VARCHAR ( 56 ) UNIQUE NOT NULL,
	expiry_status bool NOT NULL,
	updated_on   timestamp(0) ,
	created_on   timestamp(0) default  now() NOT NULL
);

CREATE TABLE scholars (
	id serial PRIMARY KEY,
	scholar_id   VARCHAR (256) UNIQUE NOT NULL,
	scholar_name VARCHAR (256),
	scholar_desc TEXT,
	scholar_mail VARCHAR (256),
	country      VARCHAR(256),
	speciality   TEXT,
	mail_status  VARCHAR (256),
	updated_on   timestamp(0) ,
	created_on   timestamp(0) default  now() NOT NULL
);


CREATE TABLE scholar_publications (
	id serial PRIMARY KEY,
	scholar_id VARCHAR (256) NOT NULL,
	paper_title TEXT,
	scholar_link TEXT,
    remote_link TEXT,
    links_status VARCHAR(300),
    extraction_status bool,
    extracted_data text,
    updated_on   timestamp(0),
	created_on   timestamp(0) default  now() NOT NULL
);

CREATE TABLE university (
	id serial PRIMARY KEY,
	uni_name VARCHAR (500) NOT NULL,
	country VARCHAR(128),
	web_address VARCHAR(128)  UNIQUE NOT NULL,
	continent VARCHAR(128),
	rank smallint,
	scholar_link  VARCHAR(128),
    updated_on   timestamp(0),
	created_on   timestamp(0) default  now() NOT NULL
);

CREATE TABLE countries (
	id serial PRIMARY KEY,
	name VARCHAR (500) NOT NULL,
	continent VARCHAR(128) NOT NULL
);
