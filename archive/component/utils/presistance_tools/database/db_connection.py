import psycopg2
from psycopg2 import Error
from utils.schol_configs import DB_USER, DB_NAME, DB_PASS, DB_HOST, DB_PORT


class DatabaseConnection:

    def __init__(self):
        self.connection = psycopg2.connect(user=DB_USER,
                                           password=DB_PASS,
                                           host=DB_HOST,
                                           port=DB_PORT,
                                           database=DB_NAME)
        self.cursor = self.connection.cursor()

    def query(self, query):
        try:
            self.cursor.execute(query)
        except Exception as e:
            print(e)
            self.connection.rollback()
        else:
            self.connection.commit()

    def select_query(self, query):
        self.cursor.execute(query)
        result = [item[0] for item in self.cursor.fetchall()]
        return result

    def select_query_general(self, query):
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        return result

    def close_db_connection(self):
        self.cursor.close()
        self.connection.close()
