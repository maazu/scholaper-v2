from utils.presistance_tools.database.db_connection import DatabaseConnection

'''
progress_type VARCHAR (500) NOT NULL,
scholar_id   VARCHAR(128) NOT NULL,
'''


class Progress:

    def __init__(self,db_connection):
        self.db = db_connection


    def get_scholar_progress(self):
        query = "SELECT last_page,uni_web_address,country FROM SCHOLAR_PROGRESS WHERE  status='false' ;";
        data = self.db.select_query_general(query)
        return data

    def update_scholar_uni_progress(self,uni_web_addree):
        query = "UPDATE SCHOLAR_PROGRESS SET status='true' WHERE uni_web_address = '{}';".format(uni_web_addree)
        self.db.query(query)


    def update_scholar_page_progress(self,last_page,uni_web_addree):
        query = "UPDATE SCHOLAR_PROGRESS SET last_page= '{}' where uni_web_address ='{}';".format(last_page,uni_web_addree)
        self.db.query(query)




