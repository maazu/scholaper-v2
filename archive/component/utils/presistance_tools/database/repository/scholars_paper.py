from utils.presistance_tools.database.db_connection import DatabaseConnection

'''
id serial PRIMARY KEY,
scholar_id         VARCHAR ( 250 ) NOT NULL,
title              VARCHAR ( 5000 ),
cited_by           INTEGER,
publish_year       INTEGER,
reference_url      TEXT,
host_url           TEXT,
ref_url_status     boolean default false,
host_url_status    boolean default false,
updated_on         timestamp(0),
created_on         timestamp(0) default  now() NOT NULL

'''


class ScholarPapers:

    def __init__(self, db_connection):
        self.db = db_connection

    def get_scholars_id_list(self):
        query = "SELECT scholar_id FROM scholars EXCEPT SELECT scholar_id FROM scholars_paper LIMIT 1000;"
        print(query)
        return self.db.select_query(query)

    def insert_new_paper(self,query):
        print(query)
        return self.db.query(query)

    def get_reference_url(self):
        query = "SELECT reference_url FROM scholars_paper where host_url is null LIMIT 5000;"
        print(query)
        return self.db.select_query(query)


    def scholar_paper_execute_query(self, query):
        print(query)
        self.db.query(query)