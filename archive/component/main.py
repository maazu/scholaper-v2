import argparse
import sys
from utils.presistance_tools.database.db_connection import DatabaseConnection
from component.proxy_loader import proxy_bouncer
from scrapers.researchers.scholars_data import start_scholar_scraping
from scrapers.reference.reference_data import start_reference_scraping
from scrapers.host_url.host_url_data import start_host_url_scraping
import threading
import time
import keyboard

app_threads_status = [False]
main_connection = DatabaseConnection()


def start_scholar_thread():
    scholar_thread = threading.Thread(target=start_scholar_scraping, args=(main_connection,))
    scholar_thread.start()


def start_reference_thread():
    reference_thread = threading.Thread(target=start_reference_scraping, args=(main_connection,))
    reference_thread.start()


def start_host_thread():
    host_url_thread = threading.Thread(target=start_host_url_scraping, args=(main_connection,))
    host_url_thread.start()


def start_email_thread():
    print("\nsquak from thread 3")
    print("\nnothing to do here ! Ask the developer to complete this !")


def start_threads():
    if app_threads_status[0] == False:
        app_threads_status[0] = True
        start_scholar_thread()
        start_reference_thread()
        start_host_thread()
        start_email_thread()
    else:
        print("Inside the main thread !")


def main_app():
    old_time = time.time()

    while True:
        if time.time() - old_time > 3600:
            print("Proxy scheduled triggered: Updating new proxies................")
            proxy_bouncer.start_proxy_thread(main_connection)
            old_time = time.time()

        if app_threads_status[0] == False:
            print("Starting all threads")
            start_threads()

        if keyboard.is_pressed("q"):
            break


if __name__ == "__main__":
    main_app_thread = threading.Thread(target=main_app)
    main_app_thread.start()
    main_app_thread.join()

    main_connection.close_db_connection()
    print("\nApplication closed ! Goodbye ")
    sys.exit()
